import { Directive, HostBinding, Input, OnInit } from '@angular/core';

@Directive({
  selector: '[ncCapacityColor]'
})
export class CapacityColorDirective implements OnInit {

  private static MIN_CAPACITY = 1;
  private static MAX_CAPACITY = 5;

  @HostBinding('style.background-color')
  @HostBinding('style.color')
  color: string;

  @Input()
  capacity: number;

  constructor() { }

  ngOnInit(): void {
    if (this.capacity === CapacityColorDirective.MIN_CAPACITY) {
      this.color = '#f44336';
    } else if (this.capacity < CapacityColorDirective.MAX_CAPACITY) {
      this.color = '#ffb74d';
    } else {
      this.color = '#80cbc4';
    }
  }
}
