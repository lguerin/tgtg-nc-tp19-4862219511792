import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';

@Directive({
  selector: '[ncHasRole]'
})
export class HasRoleDirective {

  role: string;

  constructor(private templateRef: TemplateRef<any>,
              private viewContainer: ViewContainerRef,
              private authenticationService: AuthenticationService) {
  }

  @Input()
  set ncHasRole(role: string) {
    this.role = role;
    this.updateView();
  }

  /**
   * Mets à jour la vue en fonction du matching avec le role de l'utilisateur authentifié
   */
  private updateView(): void {
    const user = this.authenticationService.getUserValue();
    if (user.roles.includes(this.role)) {
      this.viewContainer.createEmbeddedView(this.templateRef);
    } else {
      this.viewContainer.clear();
    }
  }
}
