import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { shareReplay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ReferentielService {

  categories$: Observable<Array<string>>;

  constructor(private http: HttpClient) { }

  /**
   * Récupérer les catégories des commerces
   */
  getStoreCategories(): Observable<Array<string>> {
    if (!this.categories$) {
      this.categories$ = this.http.get<Array<string>>(`${environment.baseURL}/api/referentiels/categories`)
        .pipe(
          shareReplay(1)
        );
    }
    return this.categories$;
  }
}
