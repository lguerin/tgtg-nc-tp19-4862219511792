import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'nc-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'tgtg-nc';

  constructor(private translateService: TranslateService) {
    this.translateService.addLangs(['en', 'fr']);
    this.translateService.setDefaultLang('fr');
  }
}
