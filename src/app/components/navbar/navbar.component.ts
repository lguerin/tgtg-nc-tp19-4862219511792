import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { User } from '../../models/user.model';
import { AuthenticationService } from '../../services/authentication.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'nc-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit, OnDestroy {

  user: User;
  userSubscription: Subscription;
  lang: 'fr' | 'en' = 'fr';

  constructor(private authenticationService: AuthenticationService,
              private translateService: TranslateService) { }

  ngOnInit(): void {
    this.userSubscription = this.authenticationService.user$.subscribe(u => this.user = u);
  }

  toggleLang(): void {
    this.lang = this.lang === 'fr' ? 'en' : 'fr';
    this.translateService.use(this.lang);
  }

  logout(): void {
    this.authenticationService.logout();
  }

  ngOnDestroy(): void {
    if (this.userSubscription) {
      this.userSubscription.unsubscribe();
    }
  }
}
