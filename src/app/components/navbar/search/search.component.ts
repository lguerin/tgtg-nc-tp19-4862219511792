import { Component, OnInit } from '@angular/core';
import { StoreModel } from '../../../models/store.model';
import { FormControl } from '@angular/forms';
import { TgtgService } from '../../../services/tgtg.service';
import { Router } from '@angular/router';
import { debounceTime, distinctUntilChanged, filter, switchMap } from 'rxjs/operators';

@Component({
  selector: 'nc-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  searchStore: FormControl;
  filteredStores: Array<StoreModel> = [];

  constructor(private tgtgService: TgtgService, private router: Router) { }

  ngOnInit(): void {
    this.searchStore = new FormControl();
    this.searchStore.valueChanges
      .pipe(
        filter(value => value.length > 1),
        debounceTime(500),
        distinctUntilChanged(),
        switchMap(value => this.tgtgService.search(value))
      )
      .subscribe(list => this.filteredStores = list);
  }

  clear(): void {
    this.searchStore.setValue('');
  }

  toStoreDetails(store: StoreModel): void {
    this.clear();
    this.router.navigate(['/stores', store.id]);
  }
}
